import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
/** @class mostrarAlumno:
 * 	clase estatica que hace todo el proceso y validacion para poder mostrar los datos de un alumno
 * 	en particular. Muestra su rut, su nombre con el apellido y sus notas
 */
class mostrarAlumno{
	private static String nombre;
	private static String apellido;
	private static String ramo;
	private static int nota;
	private static Scanner sc;

	
	/** metodo que muestra a un alumno en particular
	 *  pide la asignatura en donde esta el alumno y sus datos
	 * 	@param lista: lista de ramos que contiene a los alumnos
	 */
	public static void mostrar(Lista_ramos lista){
		sc=new Scanner(System.in);

		System.out.print("\nIngrese el nombre de la asignatura(q salir): ");
		ramo=leerTeclado();

		if(ramo.compareTo("q")!=0){
			if(lista.buscarRamo(ramo)==true){
				System.out.print("\nIngrese el nombre del alumno:");
				nombre=leerTeclado();
				if(nombre.compareTo("q")==0)
					return;
				
				System.out.print("Ingrese el apellido del alumno:");
				apellido=leerTeclado();

				if(apellido.compareTo("q")==0)
					return;
				
				if(comprobarAlumno(lista)==-1)
					return;						
				if(lista.mostrarAlumno(ramo, nombre, apellido)==false)
					System.out.println("Error al buscar al alumno");
			}
			else
				System.out.println("El ramo ingresado no esta registrado!");
		}
	}

	/** Metodo estatico que lee del teclado y comprueba la entrada
	 *  con el metodo estatico comprobarEntrada
	 *  	 @return la cadena leida del teclado.
	 */
	private static String leerTeclado(){
		String cadena;
		cadena=sc.nextLine();
		cadena=comprobarEntrada(cadena);
		return cadena;
	}

	/** Comprueba que el string ingresado no este vacio
	 * 	@param cadena: el string que se verificara
	 * 	@return el string, puede ser "q" o un string no vacio
	 */
	private static String comprobarEntrada(String cadena){
		cadena=cadena.trim();
		while(cadena.isEmpty()==true){
			System.out.println("Entrada invalido");
			System.out.println("Ingrese nuevamente(salir con 'q'): ");
			cadena=sc.nextLine();
			cadena=cadena.trim();
		}
		return cadena;
	}
	

	/** Este metodo comprueba si el alumno esta en el arbol de alumnos de curso, si no esta, 
	 *  da la opcion de volver a ingresar un nombre.
	 * 	@param lista: la lista de asignaturas
	 * 	@return un entero -1, si se ingresa "q" o "no" como opcion y 0 si se ingreso "si"
	 */
	private static int comprobarAlumno(Lista_ramos lista){
		int opcion;
		while(lista.buscarAlumno(ramo,nombre,apellido)==false){
			System.out.println("El alumno "+nombre+" "+apellido+", no esta registrado en la asignatura: "+ramo);
			opcion=pedirOpcion();
			if((opcion==-1) || (opcion==0))
				return -1;
		}
		return 0;
	}

	/**  metodo que pregunta al usuario si se quiere ingresar otros nombre del alumno, cuando se encuentra que este no existe
	 * 	@return -1 si se ingresa "q", 0 cuando se ingresa "no" y 1 cuando se ingresa "si"
	 */
	private static int pedirOpcion(){
		String opcion;
		System.out.println("Desea volver a ingresar? (si/no/q)");
		while(true){
			opcion=leerTeclado();
			//opcion=comprobarOpcion(opcion);
			if(opcion.compareTo("q")==0)
				return -1;
			else if(opcion.compareTo("si")==0){
				System.out.print("\nIngrese nombre del alumno: ");
				nombre=leerTeclado();
				System.out.print("\nIngrese apellido del alumno: ");
				apellido=leerTeclado();
				return 1;
			}
			else if(opcion.compareTo("no")==0)
				return 0;
			else
				System.out.println("Opcion invalida, ingrese nuevamente");
							
		}
	}
}
