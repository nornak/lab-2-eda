import java.io.*;

/**
 * Clase que contiene la lista de ramos
 * 	@param inicio: el primer elementos de la lista
 * 	@param ultimo: el ultimo elemento de la lista
 * 	@param tamanio: el largo de la lista
 */
public class Lista_ramos{	
	NodoLista inicio;
	NodoLista ultimo;
	int tamanio;
	
	/* inicializa la clase vacia */	
	Lista_ramos(){
		inicio=null;
		ultimo=null;
		tamanio=0;
	}
	/** inicializa la clase con un elemento en la lista
	 * 	@param nombre: el nombre del ramo
	 */
	Lista_ramos(String nombre){
		inicio=new NodoLista(nombre);
		ultimo=inicio;
		tamanio=1;
	}

	/** 
	 * metodo que inserta un ramo en la ultima posicion de la lista
	 * 	@param nombre: el nombre del ramo que se ingresara
	 */
	public void insertar(String nombre){
		NodoLista nuevo=new NodoLista(nombre);
		if(inicio==null){
			inicio=nuevo;
			ultimo=nuevo;
		}
		else{
			ultimo.sgte=nuevo;
			ultimo=nuevo;
		}
		tamanio++;
	}
	/** metodo que crea un fichero .in vacio
	 * 	@param nombre: nombre del fichero vacio
	 */
	public void crearFicheroVacio(String nombre){
		FileWriter fichero=null;
		PrintWriter pw=null;

		try{
			fichero=new FileWriter(nombre+".in");
			pw=new PrintWriter(fichero);
		}
		catch(Exception e){
		}finally{
			try{
				if(fichero!=null)
					fichero.close();
			}catch(Exception e2){
				e2.printStackTrace();
			}
		}
	}
	/** 
	 * Metodo que muestra en pantalla todos los ramos de la lista
	 */
	public void mostrar(){
		NodoLista temp=inicio;
		System.out.println("tamanio: "+tamanio);
		while(temp!=null){
			System.out.println("hombre curso: "+temp.nombre+"|");
			temp=temp.sgte;
		}
	}
	/**
	 * Metodo que lee el fichero de las asignaturas y las carga en la lista enlazada
	 */
	public void cargarListaArchivo(){
		File archivo=null;
		FileReader fr=null;
		BufferedReader br=null;
		String linea=null;

		try{
			archivo=new File("asignaturas.in");
			fr=new FileReader(archivo);
			br=new BufferedReader(fr);

			while((linea=br.readLine()) != null ){
				// solo guarda las linea con contenido, se salta las linea vacias
				if(linea.isEmpty()==false)
					insertar(linea);
			}
		}
		catch(Exception e){
			System.out.println("Archivo 'asignaturas.in'no encontrado.");
		}finally{
			try{
				if(fr!=null){
					fr.close();
				}
			}catch(Exception e2){
				e2.printStackTrace();
			}
		}
	}

	/**
	 * Este metodo recorre la lista de ramos y carga los alunnos del fichero
	 * llama al metodo cargarAlumnos de la clase NodoLista
	 */
	public void cargarArbolesArchivo(){
		NodoLista temp=inicio;
		for(int i=0; i<tamanio;i++){
			System.out.println("Cargado: "+temp.nombre);
			temp.cargarAlumnos();
			temp=temp.sgte;
		}
	}
	/** Este metodo guarda la lista de ramos en el archivo asignarutas.in
	 */
	public void guardarRamos(){
		NodoLista temp=inicio;
		FileWriter fichero=null;
		PrintWriter pw=null;
		try{
			fichero=new FileWriter("asignaturas.in");
			pw=new PrintWriter(fichero);
			for(int i=0;i<this.tamanio; i++){
				pw.println(temp.nombre);
				temp=temp.sgte;
			}

		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(fichero!=null){
					fichero.close();
				}
			}catch(Exception e2){
				e2.printStackTrace();
			}
		}
	}
	/** 
	 * Metodo que guarda el arbol de alumnos en el fichero correspondiente al nombre del ramo
	 * recorre cada elemento de a lista y llama al metodo guardarAlumnos2 de clase NodoLista.
	 */
	public void guardarArbol(){
		NodoLista temp=inicio;
		for(int i=0; i<tamanio;i++){
			temp.guardarAlumnos2();
			temp=temp.sgte;
		}
	}		
	/** metodo que solamente guarda el arbol del ramo ingresado como parametro
	 *  lo guarda en el archivo correspondiente a ese rama
	 * 	@param ramo: ramo que se guardara en el fichero
	 */
	public void guardarRamo(String ramo){
		NodoLista temp=inicio;
		for(int i=0; i<tamanio;i++){
			if(ramo.compareTo(temp.nombre2)==0)
				temp.guardarAlumnos2();
			temp=temp.sgte;
		}
	}
	/** 
	 * Metodo que recorre la lista de ramos y verifica si el nombre ingresado coincide 
	 * con alguno de los ramos de la lista.
	 * 	@param ramo: el nombre del ramo a comprobar
	 * 	@return un boleando indicando si se encontro el nombre del paramemtro en la lista de ramos
	 */
	public boolean buscarRamo(String ramo){
		NodoLista temp=inicio;
		for(int i=0; i<tamanio; i++){
			if(temp.nombre2.compareTo(ramo)==0)
				return true;
			temp=temp.sgte;
		}
		return false;
	}
	
	/** 
	 * Ingresa un alunno al arbol del ramo ingresado como parametro
	 * 	@param ramo: nombre del ramo
	 * 	@param rut: rut del alumno
	 * 	@param nombre: el nombre del alumno
	 * 	@param apellido: el apellido del alumno
	 * 	@return true o false, retorna true cuando encuentra 
	 * 		el ramo en la lista y false cuando no lo encuentra.
	 */
	public boolean insertarAlumno(String ramo, String rut, String nombre, String apellido){
		NodoLista temp=inicio;
		for(int i=0; i<tamanio; i++){
			if(temp.nombre2.compareTo(ramo)==0){
				temp.arbol.insertar(rut,nombre,apellido);
				return true;
			}
			temp=temp.sgte;
		}
		return false;
	}
	/** metodo que busca al ramo en la lista y llama a la funcion eliminar de la clase arbolABB
	 *	@param ramo: nombre de la asignatura en donde buscara al alumno
	 *	@param nombre: nombre del alumno
	 *	@param apellido: apellido del alumno que se eliminara
	 *	@return true si encontro el ramo en la lista, false si no lo encuentra
	 */
	public boolean eliminarAlumno(String ramo, String nombre, String apellido){
		NodoLista temp=inicio;
		for(int i=0; i<tamanio; i++){
			if(temp.nombre2.compareTo(ramo)==0){
				temp.arbol.eliminar(nombre ,apellido);
				return true;
			}
			temp=temp.sgte;
		}
		return false;
	}
	/** Metodo que lista el curso del ramo ingresado
	 * 	@param ramo: el nombre del ramo del curso
	 */
	public boolean listarCurso(String ramo){
		NodoLista temp=inicio;
		for(int i=0; i<tamanio; i++){
			if(temp.nombre2.compareTo(ramo)==0){
				temp.arbol.listar();
				return true;
			}
			temp=temp.sgte;
		}
		return false;
	}
	/** metodo que llama al metodo de la clase arbolABB para ingresar una nota a un alumno
	 * 	@param ramo: el nombre del ramo en donde se encuentra el alumno
	 * 	@param nombre: nombre del alumno
	 * 	@param apellido: apellido del alumno
	 * 	@param nota: la nota que se ingresa
	 * 	@return true si insertarNota del arbolABB devuelve true, false si devuelve false
	 */
	public boolean insertarNotaAlumno(String ramo, String nombre,String apellido, int nota){
		NodoLista temp=inicio;
		for(int i=0; i<tamanio; i++){
			if(temp.nombre2.compareTo(ramo)==0){
				if(temp.arbol.insertarNota(nombre,apellido, nota)==true)
					return true;
				else 
					return false;
			}
			temp=temp.sgte;
		}
		return false;
	}
	/** metodo que busca si existe el alumno en el ramo ingresado
	 * 	@param ramo: el nobmre del ramo en donde se buscara al alumno
	 * 	@param nombre: nombre del alumno
	 * 	@param apellido: apellido del alumno
	 * 	@return true si encuentra al alumno en el arbol de ramo y false si no lo encuentra
	 */
	public boolean buscarAlumno(String ramo, String nombre, String apellido){
		NodoLista temp=inicio;
		for(int i=0; i< tamanio;i++){
			if(ramo.compareTo(temp.nombre2)==0){
				if(temp.arbol.buscarAlumno(nombre, apellido)==true)
					return true;
				else
					return false;
			}
			temp=temp.sgte;
		}
		return false;
	}
	/** metodo que si el alumno se encuetra en el ramo ingresado como parametro
	 *  	@param ramo: nombre del ramo
	 *  	@param nombre: el nombre del alumno que se buscara en el arbol
	 *  	@param apellido: apellido del alumno.
	 *  	@return true si el alumno se encuentra en el arbol, false si el alumno no esta en el arbol
	 */
	public boolean mostrarAlumno(String ramo, String nombre, String apellido){
		NodoLista temp=inicio;
		for(int i=0; i<tamanio;i++){
			if(ramo.compareTo(temp.nombre2)==0){
				return temp.arbol.mostrarAlumno(nombre, apellido);
			}
			temp=temp.sgte;
		}
		return false;
	}
	/** busca el ramo ingresado como parametro en la lista de ramos
	 *  y llama al metodo cambiarNota del ArbolABB
	 *  	@param ramo: el ramo donde se buscara al alumno
	 *  	@param nombre: nombre del alumno
	 *  	@param apellido: apellido del alumno
	 *  	@param nota: nota que reemplazara a alguna de las del alumno
	 *  	@return true si se cambio la nota y false si el alumno no tiene notas o 
	 *  		si la nota ingresada es menor de las del alumno
	 */
	public boolean cambiarNotaAlumno(String ramo, String nombre, String apellido, int nota){
		NodoLista temp=inicio;
		for(int i=0; i<tamanio; i++){
			if(ramo.compareTo(temp.nombre2)==0){
				return temp.arbol.cambiarNota(nombre, apellido, nota);
			}
			temp=temp.sgte;
		}
		return false;
	}
	/** metodo que recorre la lista de ramos y llama al metodo optimizar() de la clase ArbolABB
	 *  para optimizar los arboles equilibrandolo, como un AVL
	 */	  	
	public void optimizarArboles(){
		NodoLista temp=inicio;
		for(int i=0; i<tamanio; i++){
			temp.arbol.optimizar();
			temp=temp.sgte;
		}
	}
	/** recorre cada lista para poder mostrar los arboles en preorden, inorden y postorden
	 */
	public void mostrarArboles(){
		NodoLista temp=inicio;
		for(int i=0; i<tamanio; i++){
			System.out.println("\n\tramo: "+temp.nombre2);
			temp.arbol.mostrar();
			temp=temp.sgte;
		}
	}
}
