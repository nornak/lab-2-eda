import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class test{
	/**
	 * Metodo estatico que usa expresiones regulares
	 * para comprobar que la cadena ingresada solo tenga numeros
	 * 	@param cadena: string que se analizara
	 * 	@return la misma cadena si no se encontraron letras, o el numero 99,
	 * 		que seria un error, ya que en el menu no esta esa opcion.
	 */
	static String soloNumeros(String cadena){
		int error=99;
		if((cadena.isEmpty()==false)){
			Pattern p=Pattern.compile("\\D"); // \D, es para buscar cualquier caracter que no sea un digito
			Matcher m=p.matcher(cadena); 
			if(m.find())    // si encuentra, significa que tiene caracteres que no son numeros, letras por ejemplo
				return error+"";
			else
				return cadena;
		}
		return error+"";
	}
	/** metodo estatico que usa expresiones regulares para
	 * comprobar si la cadena ingresada tiene el formato del rut
	 * 	@param cadena: string que contiene los caracteres que se analizaran
	 * 	@return true si tiene el formato del rut, false cuando no lo tiene
	 */
	static boolean formatoRut(String cadena){
		int error=99;
		if((cadena.isEmpty()==false)){
			Pattern p=Pattern.compile("[0-9]{7,8}-?[1-9k]"); 
			Matcher m=p.matcher(cadena); 
			if(m.find())    
				return true;
			else
				return false;
		}
		return false;
	}
	static void mostrarMenu(){
        	System.out.println("\n\tMENU\n\n1- Ingresar un nuevo curso");
		System.out.println("2- Ingresar un nuevo alumno");
		System.out.println("3- Ingresar notas a un alumno");
		System.out.println("4- Listar un curso");
		System.out.println("5- Eliminar un alumno");
		System.out.println("6- Mostrar un alumno en particular");
		System.out.println("7- Cambiar nota");
		System.out.println("8- Optimizar la busqueda");
		System.out.println("9- Salir");
		System.out.print("ingrese opcion: ");
	}
	
	public static void main(String [] args){		
		boolean menu=true;
		int opcion;
		String opcion2;
		String nombre;
		String ramo;
		String rut;
		String apellido;
		Scanner sc;
		Lista_ramos lista = new Lista_ramos();		

		lista.cargarListaArchivo();		
		lista.cargarArbolesArchivo();

		mostrarMenu();

		while(menu){
			String temp;
			sc=new Scanner(System.in);
			temp=sc.nextLine();
			temp=soloNumeros(temp);
			opcion=Integer.parseInt(temp);
			switch(opcion){
				case 1: // ingresa un nuevo curso
					System.out.println("\n\n\tINGRESAR UN NUEVO CURSO\n");
					agregarRamo.insertar(lista);
					lista.guardarRamos();
					mostrarMenu();
					break;
				
				case 2:	// ingresar un nuevo alumno
					System.out.println("\n\n\tINGRESAR UN NUEVO ALUMNO\n");
					agregarAlumno.agregar(lista);
					mostrarMenu();
					break;
				
				case 3: // ingresar notas a un alumno
					System.out.println("\n\n\tINGRESAR NUEVA NOTA");
					agregarNota.insertar(lista);
					mostrarMenu();
					break;
				
				case 4: // listar un curso
					System.out.println("\n\n\tLISTAR UN CURSO");
					sc=new Scanner(System.in);
					System.out.print("\nNombre de la asignatura: ");
					ramo=sc.nextLine();
					while(ramo.isEmpty()==true){
						System.out.println("Nombre invalido");
						System.out.println("Ingrese nuevamente(salir con 'q'): ");
						ramo=sc.nextLine();
					}
					if(ramo.compareTo("q")!=0){
						System.out.println("\nRamo: "+ramo);
						if(lista.listarCurso(ramo)!=false){
							System.out.println("\n\nPresione una tecla para continuar:");
							ramo=sc.nextLine();
						}
						else
							System.out.println("\nRamo no encontrado");
					}
					mostrarMenu();
					break;
				
				case 5: // eliminar un alumno
					System.out.println("\n\n\tELEMINAR ALUMNO");
					eliminarAlumno.eliminar(lista);
					mostrarMenu();
					break;
				
				case 6: // mostrar un alumno en particular
					System.out.println("\n\n\tMOSTRAR ALUMNO");
					mostrarAlumno.mostrar(lista);
					mostrarMenu();
					break;
				
				case 7: // cambiar nota
					System.out.println("\n\n\tCAMBIAR NOTA DE UN ALUMNO");
					cambiarNota.cambiar(lista);
					mostrarMenu();
					break;
				
				case 8: // optimizar
					System.out.println("\n\nOptimizando arboles..");
					lista.optimizarArboles();
					mostrarMenu();
					break;
				
				case 9:
					menu=false;
					break;

				default:
					System.out.println("Opcion invalida, intente nuevamente");
					break;
			}
		}
	}
}
