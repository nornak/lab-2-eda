import java.util.Scanner;
/** @class agregarRamo:
 *  	clase estatica para hacer todo el proceso de agregar un ramo a la lista
 *  	verificar si ya existe y si se quiere salir del proceso
 */
public class agregarRamo{
	private static String nombre;
	private static Scanner sc;

	/**
	 * metodo estatico que pide el nombre del ramo a agregar, haciendo una serie de verificaciones
	 * 	@param lista: la lista en donde se insertaran los ramos
	 */
	public static void insertar(Lista_ramos lista){
		System.out.println("Ingrese el nombre del curso que agrega(q salir)");
		sc=new Scanner(System.in);  // leemos del teclado
		nombre=leerTeclado();

		comprobarNombre();		

		if(nombre.compareTo("q")!=0){
			if(nombre!=null ){
				// comprobamos que no exista otro ramo con el mismo nombre
				if(lista.buscarRamo(nombre)!=true){
					lista.insertar(nombre);
					System.out.println("Ramo agregado!");
					lista.crearFicheroVacio(nombre);
				}
				else
					System.out.println("Ya se encuentra el ramo en la lista");
			}
		}
	}
	
	/**
	 * metodo estatico que comprueba si el nombre ingresado por teclado es valido
	 */
	private static void comprobarNombre(){
		nombre=nombre.trim();
		while(nombre.isEmpty()==true){
			System.out.println("Nombre invalido");
			System.out.println("Ingrese nuevamente(salir con 'q'): ");
			nombre=leerTeclado();
			nombre=nombre.trim();
		}
	}
	private static String leerTeclado(){
		sc=new Scanner(System.in);  // leemos del teclado
		return nombre=sc.nextLine();		
	}
}

