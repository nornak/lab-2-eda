import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
class cambiarNota{
	private static String ramo;
	private static String nombre;
	private static String apellido;
	private static int nota;
	private static Scanner sc;

	public static void cambiar(Lista_ramos lista){
		sc=new Scanner(System.in);
		System.out.println("Ingrese el nombre de la asignatura: ");
		ramo=leerTeclado();

		if(ramo.compareTo("q")!=0){
			if(lista.buscarRamo(ramo)==true){
				String temp;
				System.out.println("Ingrese datos del alumno:");
				System.out.println("nombre: ");
				nombre=leerTeclado();
				if(nombre.compareTo("q")==0)
					return;
				System.out.println("apellido: ");
				apellido=leerTeclado();
				if(apellido.compareTo("q")==0)
					return;
				if(comprobarAlumno(lista)==-1)
					return;
				System.out.println("Ingrese la nota de la POR");
				temp=leerTeclado();
				temp=pedirSoloNumeros(temp);
				
				if(temp.compareTo("q")==0)
					return;
				nota=Integer.parseInt(temp);
				if(lista.cambiarNotaAlumno(ramo, nombre,apellido,nota)){
					System.out.println("Nota cambiada!");
					lista.guardarRamo(ramo);
				}
				else
					System.out.println("La nota no fue cambiada!");
			}
		}
	}

	/** Metodo estatico que lee del teclado y comprueba la entrada
	 *  con el metodo estatico comprobarEntrada
	 *  	 @return la cadena leida del teclado.
	 */
	private static String leerTeclado(){
		String cadena;
		cadena=sc.nextLine();
		cadena=comprobarEntrada(cadena);
		return cadena;
	}

	/** Comprueba que el string ingresado no este vacio
	 * 	@param cadena: el string que se verificara
	 * 	@return el string, puede ser "q" o un string no vacio
	 */
	private static String comprobarEntrada(String cadena){
		cadena=cadena.trim();
		while(cadena.isEmpty()==true){
			System.out.println("Entrada invalido");
			System.out.println("Ingrese nuevamente(salir con 'q'): ");
			cadena=sc.nextLine();
			cadena=cadena.trim();
		}
		return cadena;
	}
	/** Este metodo comprueba si el alumno esta en el arbol de alumnos de curso, si no esta, 
	 *  da la opcion de volver a ingresar un nombre.
	 * 	@param lista: la lista de asignaturas
	 * 	@return un entero -1, si se ingresa "q" o "no" como opcion y 0 si se ingreso "si"
	 */
	private static int comprobarAlumno(Lista_ramos lista){
		int opcion;
		while(lista.buscarAlumno(ramo,nombre,apellido)==false){
			System.out.println("El alumno "+nombre+" "+apellido+", no esta registrado en la asignatura: "+ramo);
			opcion=pedirOpcion();
			if((opcion==-1) || (opcion==0))
				return -1;
		}
		return 0;
	}
	/**  metodo que pregunta al usuario si se quiere ingresar otros nombre del alumno
	 *   cuando se encuentra que este no existe
	 * 	@return -1 si se ingresa "q", 0 cuando se ingresa "no" y 1 cuando se ingresa "si"
	 */
	private static int pedirOpcion(){
		String opcion;
		System.out.println("Desea volver a ingresar? (si/no/q)");
		while(true){
			opcion=leerTeclado();
			//opcion=comprobarOpcion(opcion);
			if(opcion.compareTo("q")==0)
				return -1;
			else if(opcion.compareTo("si")==0){
				System.out.print("\nIngrese nombre del alumno: ");
				nombre=leerTeclado();
				System.out.print("\nIngrese apellido del alumno: ");
				apellido=leerTeclado();
				return 1;
			}
			else if(opcion.compareTo("no")==0)
				return 0;
			else
				System.out.println("Opcion invalida, ingrese nuevamente");
							
		}
	}
	/** Metodo estatico que llama al metodo comprobarNota para saber si es correcto lo ingresado, si no lo es
	 * vuelve a pedir un numero valido o hasta que se ingresa una "q"
	 * 	@param cadena: el string que se evaluara
	 * 	@return un string con un numero entre 10 y 70 o una "q"
	 */
	private static String pedirSoloNumeros(String cadena){
		if(cadena.compareTo("q")==0){
			return "q";
		}
		while(comprobarNota(cadena)==false){
			System.out.println("nota invalida, debe ser solo un numero, entre 10 y 70");
			cadena=leerTeclado();
		}
		return cadena;
	}
	/** Metodo estatico para evaluar el contenido de cadena, en el cual usamos expresiones regulares 
	 *  para poder saber si el contenido de la string tiene algun caracter o esta fuera del rango 10 y 70
	 *  	@param cadena: cadena en donde esta el mumerode la nota
	 *  	@return false si encuentra algun caracter que no sea digito o cuando la nota esta fuera de rango 
	 *  		y true cuando no encuentra caracteres o cuando esta entre 10 y 70
	 */
	private static boolean comprobarNota(String cadena){
		if((cadena.isEmpty()==false)){
			Pattern p=Pattern.compile("\\D"); 
			Matcher m=p.matcher(cadena); 
			if(m.find())    
				return false;
			else{
				if((Integer.parseInt(cadena)>=10) && (Integer.parseInt(cadena)<=70)){
					return true;
				}
				else
					return false;
			}
		}
		return false;
	}
}
