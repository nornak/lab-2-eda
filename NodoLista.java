import java.io.*;
/**
 * Clase que contiene el nombre de la asignatura y el arbol de alumnos registrados
 * 	@param nombre: el nombre de la asignatura, entre palabras hay "_"
 * 	@param nombre2: nombre del ramo, entre palabras hay espacios
 * 	@param arbol: un Clase ArbolABB que contiene los alumnos del ramo
 * 	@param sgte: el ramos siguiente de la lista
 */
public class NodoLista{
	String nombre; 
	String nombre2;
	ArbolABB arbol;
	NodoLista sgte; 

	/**
	 * Inicializa el nodo con el nombre del ramo y evalua si en 
	 * nombre contiene espacios o "_" para guardarlo donde corresponde.
	 * 	@param nombre: nombre del ramo
	 */
	NodoLista(String nombre){
		// si tiene espacios al final se quitan
		String temp=quitarEspaciosFin(nombre);

		// nombre viene con espacios entre cada palabra
		if(nombre.indexOf(" ")>0){ 
			this.nombre2=temp;
			this.nombre=temp.replace(" ","_");			
		}
		// nombre viene sin espacios pero con "_"
		else{
			this.nombre=temp;
			this.nombre2=temp.replace("_"," ");
		}
		arbol=new ArbolABB();
		sgte=null;
	}

	/** inicializa en nodo con valores nulos */
	NodoLista(){
		nombre=null;
		nombre2=null;
		sgte=null;
		arbol=null;
	}

	/** metodo para cargar los alumnos de los diferentes ramos
	 *  recorre cada linea del fichero saltandoce la linea vacias
	 *  despues cada caracter de la linea y va evaluando si contiene espacios
	 *  los espacios indican un dato de otro
	 */
	public void cargarAlumnos(){
		File archivo=null;
		FileReader fr=null;
		BufferedReader br=null;
		String linea=null;
		String dato="";
		String rut=null;
		String nombre=null;
		String apellido=null;
		String[] temp=null;
		int[] notas=new int[3];
		int cont=0;
		int contLineas=0;

		try{
			archivo=new File(this.nombre+".in");
			if(archivo.canRead()==false){
				throw new FileNotFoundException();
			}
			fr=new FileReader(archivo);
			br=new BufferedReader(fr);
			// leemos linea por linea, hasta el final
			while( (linea=br.readLine()) != null){
				contLineas++;
				// ignaramos las lineas vacias
				if(linea.isEmpty()==false){
					for(int i=0; i<linea.length(); i++){ // recorremos cada caracter de linea
						dato=dato+linea.charAt(i); // agregamos los caracteres leidos a un temporal
						if((linea.charAt(i)==' ') && (cont==0)){ 
							// si el caracter es un espacio y
							// si cont==0, cont es la cantidad de veces que se ha encontrado un espacio
							rut=dato.trim(); // con trim quitamos los espacios que estan de mas
							dato="";
							cont++;
						}
						else if((linea.charAt(i)==' ') && (cont==1)){
							nombre=dato.trim();
							dato="";
							cont++;
						}
						else if((linea.charAt(i)==' ') && (cont==2)){
							apellido=dato.trim();
							dato="";
							cont++;
						}
						else{
							if(cont==3){ // aqui llegamos a la parte donde la cantidad de espacios puede variar
								dato=linea.substring(i, linea.length()); // crea una sub cadena de datos, desde i hasta el final
								cont++;
								temp=dato.split("\\ "); // quitamos los espacios y se guarda en un arreglo de string
								for(int j=0; j<temp.length;j++){
									// se recorre cada dato de temp y se pasa a entero
									notas[j]=Integer.parseInt(temp[j]);
								}
							}						
						}
					}
				}
				// insertamos en el arbol los datos de los alumnos
				arbol.insertar(rut, nombre, apellido, notas);
				cont=0;
				dato="";
				notas=new int[3];
			}
		}catch(IOException e){
			System.out.println(this.nombre+".in no existe");
		}catch(NumberFormatException e2){
			System.out.println("Problema en leer linea: "+contLineas+" del fichero "+this.nombre+".in");
			System.out.println("lectura cancelada");
		}
		finally{
			try{
				if(fr!=null){
					fr.close();
				}
			}catch(Exception e2){
				e2.printStackTrace();
			}
		}
	}

	/** 
	 * Metodo para guardar los alumnos en el archivo
	 * llama al metodo de la clase ArbolABB para recorrer el arbol de alumnos
	 * e ir guardando cada alumno en el archivo.
	 */
	public void guardarAlumnos(){
		// el copiamos el nodo raiz del arbol
		FileWriter fichero=null;
		PrintWriter pw=null;
		try{
			fichero=new FileWriter(nombre+".in");
			pw=new PrintWriter(fichero);
			// llama al metodo guardarAlumnos de la clase ArbolABB
			this.arbol.guardarAlumnos(pw);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				fichero.close();
			}catch(Exception e2){
				e2.printStackTrace();
			}
		}	
	}
	/**
	 * Otro metodo para guardar el arbol de alumnos
	 * este metodo es diferente al anterior, el metodo de la clase arbolABB getString()
	 * recorre el arbol y va concadenando todos los datos en un String,
	 * ejemplo, en un nodo esta el rut, el nombre, el apellido y las notas, este metodo (el del arbolABB)
	 * va concadenando los datos del nodoABB en una String y este String se guarda en el archivo
	 */
	 public void guardarAlumnos2(){
		FileWriter fichero=null;
		PrintWriter pw=null;
		String datos;
		try{
			fichero=new FileWriter(nombre+".in");
			pw=new PrintWriter(fichero);
			// getString devuelve todo los alumnos del arbol en una string 
			datos=arbol.getString();
			// guarda datos en el archivo del ramo, datos es la String con todos los alumnos
			pw.print(datos);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				fichero.close();
			}catch(Exception e2){
				e2.printStackTrace();
			}
		}	
	}
	/**
	 * metodo estatico para quitar los caracteres espacios al final del string
	 * 	@param cadena: el string al cual se le quiere quitar los espacios del final
	 * 	@return  el string sin espacios al final
	 */
	public static String quitarEspaciosFin(String cadena){
		while(cadena.endsWith(" ")==true){
			cadena=cadena.substring(0, (cadena.length()-1));
		}
		return cadena;
	}
}

