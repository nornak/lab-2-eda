import java.util.*;
import java.io.*;
/**
 * Clase que contiene el arbolABB en donde estan los alumnos
 * 	 @param raiz: el arbol ABB con los alumnos
 */

public class ArbolABB{
	private NodoABB raiz;

	/**
	 *  inicializa el arbol con un nodo que contiene el rut, el nombre, el apellido
	 *  y las notas del alumno.
	 *  	@param rut: el rut del alumno.
	 *  	@param nombre: nombre del alumno.
	 *  	@param apellido: apellido del alumno.
	 *  	@param notas: arrelgo con las notas del alumno.	
	 */
	ArbolABB(String rut, String nombre, String apellido, int[] notas)
	{
		raiz=new NodoABB(rut, nombre, apellido, notas);
	}
	/* inicializa la clase con valores nulos */
	ArbolABB()
	{
		raiz=null;
	}
	public NodoABB getRaiz(){
		return raiz;
	}
	/**
	 *  inserta un alumno al arbol, en que rama se asigna depende del apellido
	 *  	@param rut: el rut de alumno a ingresar.
	 *  	@param nombre: el nombre del alumno.
	 *  	@param apellido: apellido del alumno.
	 *  	@param notas: arreglo con las notas del alumno.
	 */
	public void insertar(String rut, String nombre, String apellido, int[] notas)
	{
		if(raiz==null)
			raiz=new NodoABB(rut, nombre, apellido, notas);		
		else{
			if(apellido.compareTo(raiz.getApellido() ) < 0 )
				raiz.setIzq(insertar(rut, nombre, apellido, notas, raiz.getIzq()));
			else
				raiz.setDer(insertar(rut, nombre, apellido, notas, raiz.getDer()));
		}
	}
	/**
	 * inserta un alumno al arbol, pero sin notas.
	 *  	@param rut: el rut de alumno a ingresar.
	 *  	@param nombre: el nombre del alumno.
	 *  	@param apellido: apellido del alumno.
	 */
	public void insertar(String rut, String nombre, String apellido)
	{
		int[] notas=new int[3];
		if(raiz==null)
			raiz=new NodoABB(rut, nombre, apellido, notas);		
		else{
			if(apellido.compareTo(raiz.getApellido() ) < 0 )
				raiz.setIzq(insertar(rut, nombre, apellido, notas, raiz.getIzq()));
			else
				raiz.setDer(insertar(rut, nombre, apellido, notas, raiz.getDer()));
		}
	}
	/**
	 * inserta un alumno al arbol
	 *  	@param rut: el rut de alumno a ingresar.
	 *  	@param nombre: el nombre del alumno.
	 *  	@param apellido: apellido del alumno.
	 *  	@param notas: el arreglo con notas del alumno
	 *  	@param raiz: el nodo, en donde se insertara posiblemente
	 *  	@return el nodo raiz de este subarbol, para poder enlazarlo con el anterior
	 */
	private NodoABB insertar(String rut, String nombre, String apellido, int[] notas, NodoABB raiz)
	{
		if(raiz==null)
			raiz=new NodoABB(rut, nombre, apellido, notas);		
		else{
			if(apellido.compareTo(raiz.getApellido() ) < 0 )
				raiz.setIzq(insertar(rut, nombre, apellido, notas, raiz.getIzq() ));
			else
				raiz.setDer(insertar(rut, nombre, apellido, notas, raiz.getDer()));
		}
		return raiz;
	}

	/** 
	 * busca el mayor de los menores nodo del arbol
	 * 	@param raiz: la raiz en donde se buscara
	 * 	@return el nodo que tiene el mayor valor
	 */
	private NodoABB buscarMayorMenores(NodoABB raiz)
	{
		NodoABB aux=raiz;
		while(aux.getDer()!=null)
			aux=aux.getDer();		
		return aux;
	}

	/** 
	 * eliminar un alumno del arbol
	 * 	@param nombre: el nombre del alumno a eliminar.
	 * 	@param apellido: el apellido del alumno.
	 */
	public void eliminar(String nombre, String apellido)
	{
		if(raiz!=null)
		{
			if((nombre.compareTo(raiz.getNombre()) == 0) && (apellido.compareTo(raiz.getApellido()) == 0))
			{
				if((raiz.getIzq() == null) && (raiz.getDer() == null))
					raiz=null;				
				else if((raiz.getIzq()==null) && (raiz.getDer()!=null))
					raiz=raiz.getDer();
				else if(raiz.getDer()==null)
					raiz=raiz.getIzq();
				else{
					NodoABB aux=buscarMayorMenores(raiz.getIzq());
					raiz.setRut(aux.getRut());
					raiz.setNombre(aux.getNombre());
					raiz.setApellido(aux.getApellido());
					raiz.setNotas(aux.getNotas());
					raiz.setIzq(eliminar(aux.getNombre(), aux.getApellido(), raiz.getIzq()));
				}
			}
			else
			{
				if((apellido.compareTo(raiz.getApellido()) < 0))
					raiz.setIzq(eliminar(nombre,apellido, raiz.getIzq()));
				else
					raiz.setDer(eliminar(nombre, apellido, raiz.getDer()));		
			}
		}
	}
	/** 
	 * eliminar un nodo del arbol
	 * 	@param nombre: el nombre del alumno a eliminar.
	 * 	@param apellido: el apellido del alumno.
	 * 	@param raiz: nodo donde se buscara el alumno a eliminar
	 * 	@return la misma raiz, pero modificada
	 */
	private NodoABB eliminar(String nombre, String apellido, NodoABB raiz)
	{
		if(raiz!=null)
		{
			if((nombre.compareTo(raiz.getNombre()) == 0) && (apellido.compareTo(raiz.getApellido()) == 0))
			{
				if((raiz.getIzq() == null) && (raiz.getDer() == null))
					raiz=null;
				else if((raiz.getIzq()==null) && (raiz.getDer()!=null))
					raiz=raiz.getDer();
				else if(raiz.getDer()==null)
					raiz=raiz.getIzq();
				else{
					NodoABB aux=buscarMayorMenores(raiz.getIzq());
					raiz.setRut(aux.getRut());
					raiz.setNombre(aux.getNombre());
					raiz.setApellido(aux.getApellido());
					raiz.setNotas(aux.getNotas());
					raiz.setIzq(eliminar(aux.getNombre(), aux.getApellido(),  raiz.getIzq()));
				}
			}
			else{
				if((apellido.compareTo(raiz.getApellido()) < 0))
					raiz.setIzq(eliminar(nombre,apellido, raiz.getIzq()));
				else
					raiz.setDer(eliminar(nombre,apellido, raiz.getDer()));		
			}
			return raiz;
		}
		return raiz;
	}

	public void inorden()
	{
		if(raiz!=null)
		{
			inorden(raiz.getIzq());
			System.out.print(raiz.getApellido()+"-");
			inorden(raiz.getDer());
		}
	}
	private void inorden(NodoABB raiz)
	{
		if(raiz!=null)
		{
			inorden(raiz.getIzq());
			System.out.print(raiz.getApellido()+"-");
			inorden(raiz.getDer());
		}
	}
	public void preorden()
	{
		if(raiz!=null)
		{
			System.out.print(raiz.getApellido()+"-");
			preorden(raiz.getIzq());
			preorden(raiz.getDer());
		}
	}
	private void preorden(NodoABB raiz)
	{
		if(raiz!=null){
			System.out.print(raiz.getApellido()+"-");
			preorden(raiz.getIzq());
			preorden(raiz.getDer());
		}
	}
	public void postorden()
	{
		if(raiz!=null)
		{
			postorden(raiz.getIzq());
			postorden(raiz.getDer());
			System.out.print(raiz.getApellido()+"-");
		}
	}
	private void postorden(NodoABB raiz)
	{
		if(raiz!=null)
		{			
			postorden(raiz.getIzq());
			postorden(raiz.getDer());
			System.out.print(raiz.getApellido()+"-");
		}
	}
	
	/** 
	 * funcion recursiva que guarda todo el arbol del alumnos en el stream ingresado como parametro
	 * 	@param pw: objeto indicando donde se guardara el archivo
	 */
	public void guardarAlumnos(PrintWriter pw)
	{
		try{
			if(raiz!=null)
			{
				pw.print(raiz.prepararDatos());
				if(raiz.getIzq()!=null)
					guardarAlumnos(pw, raiz.getIzq());
				if(raiz.getDer()!=null)
					guardarAlumnos(pw, raiz.getDer());
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	/** 
	 * funcion recursiva que guarda todo el arbol del alumnos en el stream ingresado como parametro
	 * 	@param pw: objeto indicando donde se guardara el archivo
	 * 	@param raiz: ingresa el sgte nodo que se guardara
	 */
	private void guardarAlumnos(PrintWriter pw, NodoABB raiz)
	{
		try{
			pw.print(raiz.prepararDatos());
			if(raiz.getIzq()!=null)
				guardarAlumnos(pw, raiz.getIzq());
			if(raiz.getDer()!=null)
				guardarAlumnos(pw, raiz.getDer());
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	/** 
	 * metodo que concadena todos los nodos con los datos de los alumnos en una string
	 * 	@return el string concadenado
	 */
	public String getString()
	{
		String nuevo="";
		// entrega una string con los datos del alumno, nombre, apellido...etc,
		// en una linea y con el caracter salto de linea
		nuevo+=raiz.prepararDatos();
		if(raiz.getIzq()!=null)
			nuevo+=getString(raiz.getIzq());
		if(raiz.getDer()!=null)
			nuevo+=getString(raiz.getDer());
		return nuevo;
	}
	/** 
	 * metodo que concadena todos los nodos con los datos de los alumnos en una string
	 * usa los metodo de la NodoABB, para juntar los datos del alumno en una string
	 * 	@param raiz: el nodo donde seguira concadenando string
	 * 	@return el string concadenado
	 */
	private String getString(NodoABB raiz)
	{
		String nuevo="";
		nuevo+=raiz.prepararDatos();
		if(raiz.getIzq()!=null)
			nuevo+=getString(raiz.getIzq());
		if(raiz.getDer()!=null)
			nuevo+=getString(raiz.getDer());
		return nuevo;
	}
	/** metodo que comprueba si el arbol esta equilibrado, si no o esta hace los cambios para pasarlo a AVL
	 */
	public void optimizar()
	{
		int izq;
		int der;
		if(raiz!=null){
			izq=altura(raiz.getIzq())-1;
			der=altura(raiz.getDer())-1;
			raiz=detectarCaso(raiz);
		}				
	}
	/** Este metodo privado calcula el equilibrio del nodo ingresado, es la diferencia
	 *  de altura entre el hijo izquierdo menos 1 y el hijo derecho menos 1
	 * 	@param nodo: nodo al cual se le hace el calculo de las diferencias
	 * 	@return valor de la diferencia
	 */
	private int equilibrio(NodoABB nodo)
	{
		int izq;
		int der;
		int diferencia=0;
		if(nodo!=null){
			izq=altura(nodo.getIzq())-1;
			der=altura(nodo.getDer())-1;
			diferencia=der-izq;
			return diferencia;	
		}
	 	return diferencia;		
		
	}
	/** Metodo privado que analiza los hijos del nodo y evalua que rotacion realizar
	 * 	@param nodo: nodo al cual se le hace la evaluacion
	 * 	@return el nodo con las rotaciones realizadas
	 */
	private NodoABB detectarCaso(NodoABB nodo)
	{
		// hijo izquierdo
		if(equilibrio(nodo)<0){
			// subarbol izquierdo
			if(equilibrio(nodo.getIzq())<0){
				nodo=rotacionSimpleHI(nodo);
			}
			// subarbol derecho
			else if(equilibrio(nodo.getIzq())>0){
				nodo=rotacionDobleSD(nodo);
			}					
		}
		// hijo Derecho
		else if(equilibrio(nodo)>0){
			// subarbol izquierdo
			if(equilibrio(nodo.getDer())<0){
				nodo=rotacionDobleSI(nodo);
			}
			// subarbol derecho
			else if(equilibrio(nodo.getDer())>0){
				nodo=rotacionSimpleHD(nodo);
			}		
		}		
		else{
			if(nodo.getIzq()!=null)
				detectarCaso(nodo.getIzq());
			if(nodo.getDer()!=null)
				detectarCaso(nodo.getDer());
		}
		return nodo;
	}
	/** metodo que realiza la rotacion simple en el hijo izquierdo
	 * 	@param raiz: nodo en donde se produce el desequilibrio
	 * 	@return el nodo que queda como raiz
	 */
	private NodoABB rotacionSimpleHI(NodoABB raiz)
	{
		NodoABB k1=new NodoABB();
		NodoABB k2=new NodoABB();
		
		k2.copiar(raiz);
		
		k1.copiar(raiz.getIzq());		

		k2.setIzq(k1.getDer());
		k1.setDer(k2);

		return k1;
	}
	/** metodo que realiza la rotacion simple en el hijo derecho
	 * 	@param raiz: nodo en donde se produce el desequilibrio
	 * 	@return el nodo que queda como raiz
	 */
	private NodoABB rotacionSimpleHD(NodoABB raiz)
	{
		NodoABB k1=new NodoABB();
		NodoABB k2=new NodoABB();
		
		k2.copiar(raiz);
		
		k1.copiar(raiz.getDer());		

		k2.setDer(k1.getIzq());
		k1.setIzq(k2);

		return k1;
	}
	/** metodo que realiza la rotacion doble en el sub arbol derecho
	 * 	@param raiz: nodo donde esta el desequilibrio
	 * 	@return el nodo que queda como raiz
	 */
	private NodoABB rotacionDobleSD(NodoABB raiz)
	{
		NodoABB k3=new NodoABB();

		k3.copiar(raiz.getIzq());

		k3.copiar(rotacionSimpleHD(k3));
		raiz.setIzq(k3);

		raiz.copiar(rotacionSimpleHI(raiz));			

		return raiz;
	}
	/** metodo que realiza la rotacion doble en el sub arbol izquierdo
	 * 	@param raiz: nodo donde esta el desequilibrio
	 * 	@return el nodo que queda como raiz
	 */
	private NodoABB rotacionDobleSI(NodoABB raiz)
	{
		NodoABB k3=new NodoABB();

		k3.copiar(raiz.getDer());

		k3.copiar(rotacionSimpleHI(k3));
		raiz.setDer(k3);

		raiz.copiar(rotacionSimpleHD(raiz));			

		return raiz;
	}
	/** metodo que calcula la altura del nodo raiz
	 * 	@return entero con el valor de la altura
	 */
	public int altura()
	{	
		int izq=0;
		int der=0;
		if(raiz==null)
			return 0;
		else{
			izq=altura(raiz.getIzq());
			der=altura(raiz.getDer());
			if(izq>der)
				return (izq);
			else
				return (der);
		}
	}
	/** metodo que calcula la altura del nodo ingresado como parametro
	 * 	@param raiz: nodo en el cual se calculara la altura
	 */
	private int altura(NodoABB raiz)
	{
		int izq;
		int der;
		if(raiz==null)
			return 0;
		else{
			izq=altura(raiz.getIzq());
			der=altura(raiz.getDer());
			if(izq>der)
				return izq+1;
			else
				return der+1;
		}		
	}
	/** metodo que muestra por pantalla los alumnos del arbol en inorden	 
	 */
	public void listar(){
		if(raiz!=null){
			listar(raiz.getIzq());
			String temp=String.format("%s \t %s", raiz.getNombre(), raiz.getApellido());
			System.out.println(temp);
			listar(raiz.getDer());
		}
	}
	/** metodo privado que muestra por pantalla los alumnos del arbol en inorden
	 * 	@param raiz: el nodo del sgte nodo que se mostrara
	 */
	private void listar(NodoABB raiz){
		if(raiz!=null){
			listar(raiz.getIzq());
			String temp=String.format("%s \t %s", raiz.getNombre(), raiz.getApellido());
			System.out.println(temp);
			listar(raiz.getDer());
		}
	}

	/** metodo que ingresa una nota al arrelgo de nota del alumno
	 * llama al metodo ingresarNota de la clase NodoABB
	 * 	@param nombre: el nombre del alumno al cual se le ingresa la nota
	 * 	@param apellido: apellido del alumno
	 * 	@param nota: la nota que se ingresara
	 * 	@return true si se guarda la nota o false si no se pudo guardar
	 */
	public boolean insertarNota(String nombre,String apellido, int nota){
		if(raiz!=null){
			if((nombre.compareTo(raiz.getNombre())==0) && (apellido.compareTo(raiz.getApellido())==0)){
				if(raiz.ingresarNota(nota)==true)
					return true;
				else
					return false;
			}
			else{
				if(apellido.compareTo(raiz.getApellido())<0)
					insertarNota(nombre, apellido, nota, raiz.getIzq());
				else
					insertarNota(nombre, apellido, nota, raiz.getDer());
			}
		}	
		return false;
	}

	/** metodo que ingresa una nota al arrelgo de nota del alumno
	 * llama al metodo ingresarNota de la clase NodoABB
	 * 	@param nombre: el nombre del alumno al cual se le ingresa la nota
	 * 	@param apellido: apellido del alumno
	 * 	@param nota: la nota que se ingresara
	 * 	@return true si se guarda la nota o false si no se pudo guardar
	 */
	private boolean insertarNota(String nombre,String apellido, int nota, NodoABB raiz){
		if(raiz!=null){
			if((nombre.compareTo(raiz.getNombre())==0) && (apellido.compareTo(raiz.getApellido())==0)){
				if(raiz.ingresarNota(nota)==true)
					return true;
				else
					return false;
			}
			else{
				if(apellido.compareTo(raiz.getApellido())<0)
					insertarNota(nombre, apellido, nota, raiz.getIzq());
				else
					insertarNota(nombre, apellido, nota, raiz.getDer());
			}
		}	
		return false;
	}
	/** metodo que busca si exite el alumno ingresado
	 * 	@param nombre: el nombre del alumno que se busca
	 * 	@param apellido: el apellido del alumno que se busca
	 * 	@return true si lo encuentra en el arbol y false si no lo encuentra
	 */
	public boolean buscarAlumno(String nombre, String apellido){
		if(raiz!=null){
			if((nombre.compareTo(raiz.getNombre())==0) && (apellido.compareTo(raiz.getApellido())==0)){
				return true;
			}
			else{
				if(apellido.compareTo(raiz.getApellido())<0)
					return buscarAlumno(nombre, apellido, raiz.getIzq());
				else
					return buscarAlumno(nombre, apellido, raiz.getDer());
			}
		}
		return false;
	}
	/** Metodo complementario al anterior
	 * busca si el alumno se encuetra en el arbol
	 * 	@param nombre: el nombre del alumno
	 * 	@param apellido: el apellido del alumno
	 * 	@param raiz: nodo en donde se buscara el alumno
	 */
	private boolean buscarAlumno(String nombre, String apellido, NodoABB raiz){
		if(raiz!=null){
			if((nombre.compareTo(raiz.getNombre())==0) && (apellido.compareTo(raiz.getApellido())==0))
				return true;
			else{
				if(apellido.compareTo(raiz.getApellido())<0)
					return buscarAlumno(nombre, apellido, raiz.getIzq());
				else
					return buscarAlumno(nombre, apellido, raiz.getDer());
			}
		}
		return false;
	}
	/** Metodo que muestra los datos de un alumno en particular
	 *  muestra el rut, el nombre con el apellido y las notas
	 *  	@param nombre: el nobmre del alumno que se buscara en el arbol
	 *  	@param apellido: el apellido del alumno
	 *  	@return true si el alumno esta en el arbol y muestra los datos,
	 *  		false cuando el alumno no esta en el arbol
	 */
	public boolean mostrarAlumno(String nombre, String apellido){
		if(raiz!=null){
			if(nombre.compareTo(raiz.getNombre())==0 && apellido.compareTo(raiz.getApellido())==0){
				System.out.println(raiz.obtenerDatos());
				return true;
			}
			else{
				if(apellido.compareTo(raiz.getApellido())<0)
					return mostrarAlumno(nombre, apellido, raiz.getIzq());
				else
					return mostrarAlumno(nombre, apellido, raiz.getDer());
			}
		}
		return false;
	}
	/** Metodo privado que muestra los datos de un alumno en particular
	 *  muestra el rut, el nombre con el apellido y las notas
	 *  	@param nombre: nombre del alumno
	 *  	@param apellido: el apellido del alumno
	 *  	@param raiz: raiz en donde se buscara al alumno
	 *  	@return true si el alumno esta en el arbol y muestra los datos,
	 *  		false cuando el alumno no esta en el arbol
	 */
	private boolean mostrarAlumno(String nombre, String apellido, NodoABB raiz){
		if(raiz!=null){
			if(nombre.compareTo(raiz.getNombre())==0 && apellido.compareTo(raiz.getApellido())==0){
				System.out.println(raiz.obtenerDatos());
				return true;
			}
			else{
				if(apellido.compareTo(raiz.getApellido())<0)
					return mostrarAlumno(nombre, apellido, raiz.getIzq());
				else
					return mostrarAlumno(nombre, apellido, raiz.getDer());
			}
		}
		return false;
	}
	/** metodo recursivo que busca al estudiante en el arbol y 
	 *  llama al metodo de cambiarNota de la clase NodoABB
	 * 	@param nombre: el nombre del estudiante al cual se le cambiara la nota
	 * 	@param apellido: el apellido del estudiante
	 * 	@param nota: la posible nota que se cambiara
	 * 	@return true cuando se cambia la nota y false cuando no se cambio la nota o
	 * 		cuando no encontro al alumno
	 */
	public boolean cambiarNota(String nombre, String apellido, int nota){
		if(raiz!=null){
			if(nombre.compareTo(raiz.getNombre())==0 && apellido.compareTo(raiz.getApellido())==0){
				if(raiz.cambiarNota(nota))
					return true;
				else
					return false;
			}
			else{
				if(apellido.compareTo(raiz.getApellido())<0)
					return cambiarNota(nombre, apellido, nota, raiz.getIzq());
				else
					return cambiarNota(nombre, apellido, nota, raiz.getDer());
			}
		}
		return false;
	}
	/** metodo privado que busca al estudiante en el arbol y 
	 *  llama al metodo de cambiarNota de la clase NodoABB
	 * 	@param nombre: el nombre del estudiante al cual se le cambiara la nota
	 * 	@param apellido: el apellido del estudiante
	 * 	@param nota: la posible nota que se cambiara
	 * 	@param raiz: nodo en donde se buscara al estudiante
	 * 	@return true cuando se cambia la nota y false cuando no se cambio la nota o
	 * 		cuando no encontro al alumno
	 */
	private boolean cambiarNota(String nombre, String apellido, int nota, NodoABB raiz){
		if(raiz!=null){
			if(nombre.compareTo(raiz.getNombre())==0 && apellido.compareTo(raiz.getApellido())==0){
				if(raiz.cambiarNota(nota))
					return true;
				else
					return false;
			}
			else{
				if(apellido.compareTo(raiz.getApellido())<0)
					return cambiarNota(nombre, apellido, nota, raiz.getIzq());
				else
					return cambiarNota(nombre, apellido, nota, raiz.getDer());
			}
		}
		return false;
	}
	/** metodo que imprimer los nodos del arbol de la forma preorden, inorden y postorden
	 */
	public void mostrar(){
		System.out.println("preorden:");
		preorden();		
		System.out.println("\ninorden:");
		inorden();
		System.out.println("\npostorden:");
		postorden();
	}
}
