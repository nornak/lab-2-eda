import java.util.Scanner;
/** @class agregarRamo:
 *  	clase estatica para hacer todo el proceso de agregar un ramo a la lista
 *  	verificar si ya existe y si se quiere salir del proceso
 */
public class agregarAlumno{
	private static String nombre;
	private static String apellido;
	private static String ramo;
	private static String rut;
	private static Scanner sc;

	/**
	 * metodo estatico que pide el nombre del ramo a agregar, haciendo una serie de verificaciones
	 * 	@param lista: la lista en donde se insertaran los ramos
	 */
	public static void agregar(Lista_ramos lista){
		String opcion;
		sc=new Scanner(System.in);

		System.out.println("Ingrese el nombre del curso en donde agregara al alumno(q salir)");
		ramo=leerTeclado();

		if(ramo.compareTo("q")!=0){
			if(lista.buscarRamo(ramo)==true){
				System.out.println("Ingrese los datos del alumno");
				System.out.print("nombre: ");
				nombre=leerTeclado();

				if(nombre.compareTo("q")==0)
					return;
				System.out.print("apellido: ");
				apellido=leerTeclado();

				if(apellido.compareTo("q")==0)
					return;
				System.out.print("rut: ");			
				rut=leerTeclado();

				if(rut.compareTo("q")==0)
					return;

				if(confirmacion()==-1)
					return;
				if(lista.insertarAlumno(ramo, rut, nombre, apellido)==true){
					System.out.println("Alumno agregado con exito");
					lista.guardarRamo(ramo);
				}
				else
					System.out.println("Error al ingresar alumno");

			}		
			else
				System.out.println("La asignatura ingresada no esta registrada");
		}
	}
	
	/** Comprueba que el string ingresado no este vacio
	 * 	@param cadena: el string que se verificara
	 * 	@return el string, puede ser "q" o un string no vacio
	 */
	private static String comprobarEntrada(String cadena){
		cadena=cadena.trim();
		while(cadena.isEmpty()==true){
			System.out.println("Entrada invalido");
			System.out.println("Ingrese nuevamente(salir con 'q'): ");
			cadena=sc.nextLine();
			cadena=cadena.trim();
		}
		return cadena;
	}
	/** Metodo estatico que lee del teclado y comprueba la entrada
	 *  con el metodo estatico comprobarEntrada
	 *  	 @return la cadena leida del teclado.
	 */
	private static String leerTeclado(){
		String cadena;
		cadena=sc.nextLine();
		cadena=comprobarEntrada(cadena);
		return cadena;
	}

	
	/**  metodo que pregunta al usuario si se quiere ingresar otros nombre del alumno,
	 *    cuando se encuentra que este no existe
	 * 	@return -1 si se ingresa "q", 0 cuando se ingresa "no" y 1 cuando se ingresa "si"
	 */
	private static int confirmacion(){
		String opcion;
		System.out.println("\nConfirme si los datos estan correctos(si/no/q)");
		while(true){
			opcion=leerTeclado();
			if(opcion.compareTo("q")==0)
				return -1;
			else if(opcion.compareTo("no")==0){
				System.out.print("\nIngrese nombre del alumno: ");
				nombre=leerTeclado();
				System.out.print("\nIngrese apellido del alumno: ");
				apellido=leerTeclado();
				System.out.print("\nIngrese rut del alumno: ");
				rut=leerTeclado();
				return confirmacion();
			}
			else if(opcion.compareTo("si")==0)
				return 0;
			else
				System.out.println("Opcion invalida, ingrese nuevamente");
		}
	}
}

