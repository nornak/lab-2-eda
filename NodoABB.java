import java.util.*;
/**
 * Clase que contiene los datos de los alumnos:
 * 	@param rut: el rut del alumno
 * 	@param nombre: el nombre del alumno
 * 	@param apellido: el apellido del alumno
 * 	@param notas: arreglo con las notas del alumno
 * 	@param izq: hijo izquierdo de NodoABB
 * 	@param der: hijo derecho de NodoABB
 */

public class NodoABB{
	private String rut;
	private String nombre;
	private String apellido;
	private int notas[];
	private NodoABB izq;
	private NodoABB der;

	// inicializa el nodo con el rut, el nombre, el apellido y las notas del alumno
	NodoABB(String rut, String nombre, String apellido, int[] notas){
		this.rut=rut;
		this.nombre=nombre;
		this.apellido=apellido;
		this.notas=notas;
		this.izq=null;
		this.der=null;
	}
	// inicializa el nodo con rut, el nombre y el apellido del alumno, las notas las inicializa vacias
	NodoABB(String rut, String nombre, String apellido){
		this.rut=rut;
		this.nombre=nombre;
		this.apellido=apellido;
		this.notas=new int[3];
		this.izq=null;
		this.der=null;
	}
	// inicializa el nodo con valores nulos
	NodoABB(){
		this.rut=null;
		this.nombre=null;
		this.apellido=null;
		this.notas=new int[3];
		izq=null;
		der=null;
	}

	public NodoABB getIzq(){
		return izq;
	}
	public NodoABB getDer(){
		return der;
	}
	public String getRut(){
		return rut;
	}
	public String getNombre(){
		return nombre;
	}
	public String getApellido(){
		return apellido;
	}
	public int [] getNotas(){
		return notas;
	}
	public void setIzq(NodoABB nodo){
		izq=nodo;
	}
	public void setDer(NodoABB nodo){
		der=nodo;
	}
	public void setNombre(String nombre){
		this.nombre=nombre;
	}
	public void setApellido(String apellido){
		this.apellido=apellido;
	}
	public void setRut(String rut){
		this.rut=rut;
	}
	public void setNotas(int[] notas){
		this.notas=notas;
	}
	/** 
	 * las notas que estan en el array, las pasa a String
	 *	@return: devuelve el String con las notas.
	 */
	private String pasarNotas(){
		String notas="";
		for(int i=0; i<3; i++){
			// aqui comprabamos si el numero es distinto de cero, para poner solamente un cero en el string
			if(this.notas[i]!=0){
				if(i==2)
					notas+=this.notas[i];
				else
					notas+=this.notas[i]+" ";
			}
			else{
				notas+=this.notas[i];
				i=3; // salimos
			}

		}
		return notas;
	}
	/**
	 * Funcion que junta todos los datos del alumno en una sola String
	 * para guardarla en el fichero
	 * 	@return el String con todos los datos
	 */
	public String prepararDatos(){
		String nuevo="";
		return nuevo+=rut+" "+nombre+" "+apellido+" "+pasarNotas()+"\n";
	}
	/** metodo que retorna los datos del alumno en una string
	 *  para mostrarla en pantalla
	 * 	@return string con los datos del alumno
	 */
	public String obtenerDatos(){
		String nuevo="";
		return nuevo+="Rut: "+rut+" Nombre: "+nombre+" "+apellido+" Notas: "+pasarNotas()+"\n";
	}
	/** Funcion para copiar los datos de un Nodo a otro nodo
	 * 	@param nodo: el nodo que se quiere copiar
	 */ 
	public void copiar(NodoABB nodo){
		setNombre(nodo.getNombre());
		setApellido(nodo.getApellido());
		setRut(nodo.getRut());
		setNotas(nodo.getNotas());
		setIzq(nodo.getIzq());
		setDer(nodo.getDer());
		
	}
	/**
	 * Funcion para comprobar si en el arreglo hay espacio para otra nota
	 * 	@return: un boleano, true hay espacio para otra nota, false no hay espacio
	 */
	private boolean comprobarNotas(){
		int cont=0;
		for(int i=0; i<3; i++){
			if(this.notas[i]!=0){
				cont++;
			}
		}		
		if(cont==3)
			return false;
		else
			return true;
	}
	/**
	 * Metodo para insertar una nota en el arreglo
	 * 	@param nota: la nota que se quiere ingresar al arreglo
	 * 	@return: un boleano indicando si se ingreso o no la nota, true si se pudo ingresar, false no fue posible
	 */
	public boolean ingresarNota(int nota){
		if(comprobarNotas()==true){
			for(int i=0; i<3; i++){
				if(this.notas[i]==0){
					this.notas[i]=nota;
					return true;
				}
			}
			return false;
		}
		else
			return false;
	}

	/** cambia la peor nota por la nota ingresada solo si la nota ingresada es mayor 
	 *  que la menor nota que posee el estudiante
	 *  	@param nota: la nota que se cambiara por la peor nota
	 *  	@return true si se cambia la nota, false si no se cambia
	 */
	public boolean cambiarNota(int nota){
		int pos=posMenorNota();
		if(pos!=-1){
			int menorNota=this.notas[pos];
			if(menorNota<nota){
				this.notas[pos]=nota;
				return true;
			}
			else
				return false;
		}else
			return false;
	}

	/** Metodo que busca la posicion de la menor nota
	 * 	@return un numero entre 0 y 2 indicando la posicion de la menor nota en el arreglo
	 * 		y -1 cuando no hay menor nota, ocurriria cuando todas las notas son 70 o 
	 * 		cuando no tiene notas
	 */
	private int posMenorNota(){
		int aux=70;
		int pos=-1;
		for(int i=0; i<3; i++){
			if(notas[i]<aux && notas[i]!=0){
				aux=notas[i];
				pos=i;
			}
		}
		return pos;
	}
}
